#asunciones que hago sobre la estructura de los datos:
# recvTime es la variable tiempo 
# tiene un filtro $gt obligado y otro opcional $lte

from typing import List, Optional,Union
from pymongo import MongoClient
import requests
from datetime import datetime
#lectura de datos de base de datos MongoDB

def read_mongo_find(host_mongo:str, port_mongo:Union[str,int], database_mongo:str, collection_mongo:str, user_mongo:str,
                passwd_mongo:str, filter_mongo: dict, fields_select: Optional[List[str]] = None,filter_id: bool = True) -> Optional[List]:


    #primero creamos la conexion para no perder el tiempo y que falle cuanto antes si no puede conectar
    client = MongoClient(f"mongodb://{user_mongo}:{passwd_mongo}@{host_mongo}:{port_mongo}/{database_mongo}")
    db = client[database_mongo]
    collection = db[collection_mongo]
    results = []

    #ahora formateamos el filtro y los campos para meterselos al método find de mongo
    if fields_select:
        fields_dict = {field: 1 for field in fields_select}
        
    else:
        fields_dict={}

    if filter_id:
        
        fields_dict["_id"]=0
    # formateamos las fechas como las espera pymongo

    #el greater than tiene que estar siempre

    try:
        filter_mongo["recvTime"]["$gt"]=datetime.strptime(filter_mongo["recvTime"]["$gt"],r"%Y-%m-%dT%H:%M:%S") #probamos un formateo de fechas con "-"
    except ValueError:
        #esto de poner try's dentro de try's es muy cutre, pero asi pone que se hace a veces :S
        try: 
            filter_mongo["recvTime"]["$gt"]=datetime.strptime(filter_mongo["recvTime"]["$gt"],r"%Y-%m-%dT%H:%M:%S") #probamos un formateo de fechas con "/"
        except ValueError:
            print("Algun error al convertir la fecha del filtro en formato datetime de python.\n Deberia ser:YYYY-mm-ddTHH:MM:SS ó YYYY/mm/ddTHH:MM:SS")

    #el least than puede estar o no
    if filter_mongo["recvTime"]["$lte"] in filter_mongo["recvTime"]:
        try:
            filter_mongo["recvTime"]["$lte"]=datetime.strptime(filter_mongo["recvTime"]["$lte"],r"%Y-%m-%dT%H:%M:%S") #probamos un formateo de fechas con "-"
        except ValueError:
            #esto de poner try's dentro de try's es muy cutre, pero asi pone que se hace a veces :S
            try: 
                filter_mongo["recvTime"]["$lte"]=datetime.strptime(filter_mongo["recvTime"]["$lte"],r"%Y-%m-%dT%H:%M:%S") #probamos un formateo de fechas con "/"
            except ValueError:
                print("Algun error al convertir la fecha del filtro en formato datetime de python.\n Deberia ser: YYYY-mm-ddTHH:MM:SS ó YYYY/mm/ddTHH:MM:SS")

        #y nos aseguramos que este valor es mator que la otra fecha
        assert filter_mongo["recvTime"]["$lte"]>filter_mongo["recvTime"]["$gt"],"La fecha $gt es mayor que la de $lte"

    #la query propiamente dicha
    query = collection.find(filter=filter_mongo,projection=fields_dict)

    #ponemos en una lista los documentos devueltos
    if query.count()>0:
        for doc in query:
            results.append(doc)
    else:
        print("La despuesta de la query está vacía.")

    client.close()
    if results:
        return results
    else:
        print("No se han devuelto resultados de la consulta.")
        return []
    

def read_mongo_aggregate(host_mongo:str, port_mongo:Union[str,int], database_mongo:str, collection_mongo:str, user_mongo:str,
                    passwd_mongo:str,pipeline_mongo:List[dict]) -> List:
    #esta función está para que apliquemos una pipeline de mongo en lugar de unsimple find.
    #en principio esto es lo que vamos a usar para cargar los datos agregados en lugar de traernos de mongo todo los datos y agregarlos en python.
    #con esta función la otra de read_mongo_find quedaría deprecada, pero la dejo por si se quisiera para uso futuro

    # Conectarse a la base de datos de Mongo
    client = MongoClient(f"mongodb://{user_mongo}:{passwd_mongo}@{host_mongo}:{port_mongo}/{database_mongo}")
    db = client[database_mongo]
    collection = db[collection_mongo]

    # Crear la fecha de inicio y fin para el filtro en el $match
    #debemos de pasarlas a tipo fecha de python
    #ESTO SE EJECUTA SI VIENEN EN TIPO TEXTO

    if type(pipeline_mongo[0]["$match"]["recvTime"]["$gt"])==str:
        try:
            pipeline_mongo[0]["$match"]["recvTime"]["$gt"]=datetime.strptime(pipeline_mongo[0]["$match"]["recvTime"]["$gt"],r"%Y-%m-%dT%H:%M:%S") #probamos un formateo de fechas con "-"
        except ValueError:
            #esto de poner try's dentro de try's es muy cutre, pero asi pone que se hace a veces :S
            try: 
                pipeline_mongo[0]["$match"]["recvTime"]["$gt"]=datetime.strptime(pipeline_mongo[0]["$match"]["recvTime"]["$gt"],r"%Y-%m-%dT%H:%M:%S") #probamos un formateo de fechas con "/"
            except ValueError:
                print("Algun error al convertir la fecha $gt del filtro en formato datetime de python.\n Deberia ser: YYYY-MM-DD ó YYYY/MM/DD")
    elif type(pipeline_mongo[0]["$match"]["recvTime"]["$gt"])==datetime:
        pass
    else:
        print("Error con el tipo de la fecha $gt de la pipeline")


    if type(pipeline_mongo[0]["$match"]["recvTime"]["$lte"])==str:
        try:
            pipeline_mongo[0]["$match"]["recvTime"]["$lte"]=datetime.strptime(pipeline_mongo[0]["$match"]["recvTime"]["$lte"],r"%Y-%m-%dT%H:%M:%S") #probamos un formateo de fechas con "-"
        except ValueError:
            #esto de poner try's dentro de try's es muy cutre, pero asi pone que se hace a veces :S
            try: 
                pipeline_mongo[0]["$match"]["recvTime"]["$lte"]=datetime.strptime(pipeline_mongo[0]["$match"]["recvTime"]["$lte"],r"%Y-%m-%dT%H:%M:%S") #probamos un formateo de fechas con "/"
            except ValueError:
                print("Algun error al convertir la fecha $lte del filtro $match en formato datetime de python.\n Deberia ser: YYYY-MM-DD ó YYYY/MM/DD")
    elif type(pipeline_mongo[0]["$match"]["recvTime"]["$lte"])==datetime:
        pass
    else:
        print("Error con el tipo de la fecha $lte de la pipeline")




    # Ejecutar el aggregate
    results=[]
    cursor = collection.aggregate(pipeline=pipeline_mongo)
    #ponemos en una lista los documentos devueltos
    for doc in cursor:
        results.append(doc)

    client.close()
    if results:
        return results
    else:
        print("No se han devuelto resultados de la consulta.")
        return []






def send_data_http(endpoint: str, list_payloads: List[dict],params:dict, verbose: bool = True, headers:dict={"Content-Type": "application/json"}):
    for i,payload in enumerate(list_payloads):
        try:
            response = requests.post(endpoint, json=payload,headers=headers,params=params)
            if (response.status_code != 201) & (response.status_code != 200):
                if verbose:
                    print(f"Error {response.status_code} on data {i}")
        except requests.exceptions.RequestException as e:
            if verbose:
                print(f"Error {e} on data {i}")

