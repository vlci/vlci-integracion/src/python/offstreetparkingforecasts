#funciones para crear y evaluar los diferentes modelos
from scipy.optimize import curve_fit
import numpy as np
#from scipy.optimize import OptimizeWarning
from typing import Optional, List
import warnings
from utils import shift

warnings.filterwarnings("ignore", message="Covariance") # esto es porque el aguste curve_fit no nos imprima errores de Covariance not calculated


#el formato que queremos para cualquier función que esté aquí para predecir, es:
#input: entra el X que conocemos, y su Y, y opcional entran un X_predict o un n_day_predict
#output: si no se le piden predicciones, se devuelve el modelo entrenado, un modelo que es una 
# funcion lista para darle esos campos. Si se le indican, entonces tenemos que devolver la prediccion.

#IMPORTANTE, para hacer los fits, hemos modificado el eje X, las horas mayor o igual que 10 han sido -9, y el resto +15
#para ahora hacer predicciones sobre horas correctamente, tenemos que hacer lo contrario
# para horas de 1 a 15, tenemos que sumarle 9 y de 16 a 24 tenemos que restar 15.




#============== 2 GAUSSIANAS SUMADAS =====================0

def forecast_gaussian(X:np.ndarray,Y:np.ndarray,X_pred:Optional[np.ndarray]=None,n_days_pred:Optional[int]=None,
                        x_shift:Optional[int]=9,p0:Optional[List]=None,maxfev:Optional[int]=1200):
    #step 0 hacemos la transformación de las horas segun hemos indicado

    #step 1, fit de las gaussianas
    #podría darse el caso que no es capaz de ajustar correctamente, en el tiempo que le damos
    #debemos manejar esta excepción
    #si le pedimos que devuelva n days, nos devuelve desde las 1 del día, hasta las 24, n veces

    def gaussian(x, a, b, c, d, e,f):
        x=shift(x,limit=x_shift)
        return a*np.exp(-((x-b)/c)**2) + d*np.exp(-((x-e)/f)**2)



    if X_pred is not None and n_days_pred is not None:
        raise ValueError("Solo se permite un argumento, X_pred o n_days_pred, no ambos.")


    #es muy importante que tambien hagamos el shift de los valores Y, sino no preservamos la 
    #correspondencia x->y
    #Y=shift(Y)
    popt, _ = curve_fit(gaussian, X, Y,p0=p0,maxfev=maxfev)
    if len(popt)<0:
        raise RuntimeError
    #si no se ha podido ajustar, entonces tenemos que buscar un plan B
    # un posible plan B podría ser darle otras opciones para P0, y un plan C podría ser 
    # poner unos valores que siempre han ido bien, y que es lo último que funcionó.


    #funcion que nos ayuda a predecir
    def predict(x):
        return gaussian(x,*popt)

    #si todo ha ido bien, llegamos a la segunda parte donde predecimos si nos lo piden, sino devolvemos 
    #un funcion que lo hará
    if X_pred is not None:
        return predict(X_pred)

    elif n_days_pred is not None:

        X_pred=np.tile(np.arange(0,24),n_days_pred)
        return predict(X_pred)

    else:
        return predict #devolvemos la funcion para predecir talcual


#============== 2 GAUSSIANAS SEPARADAS =====================0

def forecast_gaussian2(X:np.ndarray,Y:np.ndarray,X_pred:Optional[np.ndarray]=None,n_days_pred:Optional[int]=None,
                        x_shift:Optional[int]=9,p0:Optional[List]=[0.9,5,14,0.98,14,19,8],maxfev:Optional[int]=1200):
    #step 0 hacemos la transformación de las horas segun hemos indicado

    #step 1, fit de las gaussianas
    #podría darse el caso que no es capaz de ajustar correctamente, en el tiempo que le damos
    #debemos manejar esta excepción
    #si le pedimos que devuelva n days, nos devuelve desde las 1 del día, hasta las 24, n veces

    def gaussian2(x, a, b, c, d, e, f ,g):
        x=shift(x,limit=x_shift) #esto para shiftear los x
        #ahora la prediccion tiene un parámetro en el where
        return np.where(x<=g ,a*np.exp(-((x-b)/c)**2), d*np.exp(-((x-e)/f)**2))



    if X_pred is not None and n_days_pred is not None:
        raise ValueError("Solo se permite un argumento, no ambos.")


    popt, _ = curve_fit(gaussian2, X, Y,p0=p0,maxfev = maxfev)
    if len(popt)<0:
        raise RuntimeError

    #funcion que nos ayuda a predecir
    def predict2(x):
        return gaussian2(x,*popt)

    #si todo ha ido bien, llegamos a la segunda parte donde predecimos si nos lo piden, sino devolvemos 
    #un funcion que lo hará
    if X_pred is not None:
        return predict2(X_pred)

    elif n_days_pred is not None:

        X_pred=np.tile(np.arange(0,24),n_days_pred)
        return predict2(X_pred)

    else:
        return predict2 #devolvemos la funcion para predecir talcual



#==============POLY===================0

def forecast_poly(X:np.ndarray,Y:np.ndarray,X_pred:Optional[np.ndarray]=None,n_days_pred:Optional[int]=None,
                        x_shift:Optional[int]=9,p0:Optional[List]=[1,-0.1,-1.7,3.6,-0.1,-31,7],maxfev:Optional[int]=1200):
    #step 1, fit de las gaussianas
    #podría darse el caso que no es capaz de ajustar correctamente, en el tiempo que le damos
    #debemos manejar esta excepción


    def poly(x,a1,b1,c1,a2,b2,c2,d):
        x=shift(x,limit=x_shift) #esto para shiftear los x
        return np.where(x<=d,a1*x+b1*x**2+c1,a2*x+b2*x**2+c2)

    if X_pred is not None and n_days_pred is not None:
        raise ValueError("Solo se permite un argumento, no ambos.")


    popt, _ = curve_fit(poly, X, Y,p0=p0,maxfev=maxfev)
    if len(popt)<0:
        raise RuntimeError

    #funcion que nos ayuda a predecir
    def predict(x):
        return poly(x,*popt)

    #si todo ha ido bien, llegamos a la segunda parte donde predecimos si nos lo piden, sino devolvemos 
    #un funcion que lo hará
    if X_pred is not None:
        return predict(X_pred)

    elif n_days_pred is not None:

        X_pred=np.tile(np.arange(0,24),n_days_pred)
        return predict(X_pred)

    else:
        return predict #devolvemos la funcion para predecir talcual


