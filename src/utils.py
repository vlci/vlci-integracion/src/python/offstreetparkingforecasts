from datetime import datetime,timedelta,time
import json 
import numpy as np
from argparse import ArgumentParser
from typing import Union
import matplotlib.colors as mcolors

#cargar argumentos 

def init_parser():
    parser = ArgumentParser()
    parser.add_argument('-p','--parking', help='parking_id', choices=["5","8","75","120"], required=True)
    return parser

# debemos elegir el punto de shift, por ejemplo, que las 10 sean la 1 para 
# la gausiana --> 10-9, 11-9 ... 1+15 = 16,2+15 = 17 ... 9 + 15 = 24
# pero si quisieramos si quisieramos que fueran las 9 las nuevas 1, entonces
# tendriamos: 9 - 8 = 1, 10-8=2 ... 1+16= 17, 2 +16 = 16 ... 8+16=24 y así
#LIMIT=9 #solo hace falta cambiar esto
#MINUS=LIMIT
#PLUS=24-MINUS + 1 

def shift(x:Union[list,np.ndarray],limit:int=9):
    minus=limit #esto es asi por que vamos de 0 a 23, si fueramos de 1 a 24, seria minus=limit-1, para tener limit-minus=1
    plus=24-minus + 1 
    if type(x)==list:
        x=np.array(x)
    return np.array([x[i]-minus if x[i]>=limit else x[i]+plus for i in range(x.shape[0]) ])



def lista_fechas(num_preds:int,last_datetime:datetime,intervals:str="H"):
    #esta función existe para crear la lista de las fechas de predicciones 
    #hemos obtenido una serie de predicciones indicando e.g. 2 días de preddicones
    # y tenemos 48 valores sin una correspondiente fecha bien formateada
    #le indicamos las lecturas, y el último día de datos disponible.
    # si el ultimo día-hora con lecturas es de madrugada, tomamos ese mismo día, sino, se toma el día siguiente

    if last_datetime.hour in [0,1,2,3,4,5,6,7]:
    #si es de madrugada tomamos ese dia
        inicio = last_datetime.replace(hour=0,minute=0,second=0)
    else:
        #sino si tomamos el siguiente día, porque asumo que se hace para el día siguiente
        inicio = datetime.combine((last_datetime.date()+ timedelta(days=1) ), time(0, 0, 0))

    #para sacar el siguiente día
    
    if intervals =="H":
        #no tenemos otra opcion, por ahora
        lista_fechas = [inicio + timedelta(hours=h) for h in range(0,num_preds)] 
    return lista_fechas


def cargar_from_json(file_dir:str,required_keys:list):
    #esto cargará los secrets necesarios desde un file json que se bajará desde gitlab/vlci-secrets
    #el fichero ya está en el workspace a la hora de cargarlo
    #le indicamos la ruta del archivo, y las propiedades que debemos cargar, así si no está 
    #avisamos pronto del error
    with open(file_dir,"rt") as fil:
        result=json.load(fil)
        
    #comprobamos que estan las propiedades requeridas
    try:
        check_keys(result, required_keys)
    except NewKeyError as e:
        print(f"Error. Los campos {e.missing_keys}, no están entre los disponibles.")
    else : 
        #ejecutamos esto si el try no fallo
        return {key:result[key] for key in required_keys}




######Esto es para la funcion cargar_secrets_from_json(), para comprobar que están los campos necesarios
class NewKeyError(KeyError):
    #creamos esta excepcion para poder devolver todas las KEYS que no están, sino solo podráimos devolver
    #una en el momento que data["key"] no existiera, y se deolveria una KeyError, con el argumento .args
    def __init__(self, missing_keys):
        self.missing_keys = missing_keys


def check_keys(data:dict, required_keys:list):
    #raise NewKeyError si no tenemos algunas de las claves solicitadas
    missing_keys = []
    for key in required_keys:
        if key not in data.keys():
            missing_keys.append(key)
    if missing_keys:
        raise NewKeyError(missing_keys)


#paleta de colores para el diagrama de barras
def y_to_colors(Y):
    #si tenemos Y solo con 1 valor, va a dar error, porque no tiene variacion para hacer el rango de colores

    unique_values = np.unique(Y)
    if len(unique_values) == 1:
        colors = [mcolors.hex2color('#B6D7B6')] * len(Y)
    else:
        Y_norm = (Y - Y.min()) / (Y.max() - Y.min())
        # Definir los extremos del mapa de colores
        colors = ['#F9A7B0', '#FCD0BA', '#ECE09E', '#B6D7B6']  # Códigos hexadecimales de colores pastel
        boundaries = [Y_norm.min(), np.percentile(Y_norm, 33), np.percentile(Y_norm, 67), Y_norm.max()]

        # Crear el mapa de colores
        cmap = mcolors.LinearSegmentedColormap.from_list('my_cmap', list(zip(boundaries, colors)))

        # Asignar un color a cada valor de Y
        norm = mcolors.Normalize(vmin=Y_norm.min(), vmax=Y_norm.max())
        colors = [cmap(norm(y)) for y in Y_norm]

    return colors