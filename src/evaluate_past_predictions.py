# Arturo Sirvent Fresneda
# Idrica, proyecto Valencia Smart City
# 01/03/2023

# Este script esta para que se creen unos gráficos que muestren que tal ha ido la ultima prediccion de la 
# que ya se tienen datos (pues es el día pasado).


#=============== Librerias ===============
from os.path import dirname,abspath,join
BASE_DIR = abspath(join(dirname(__file__), '..'))
#"/opt/etls/sc_vlci/PRE/py_oci_parkings_forecast"

import sys
sys.path.append(f"{BASE_DIR}/src")

# basicas
from datetime import datetime,timedelta
import os 
import pandas as pd 
import numpy as np
import re
import matplotlib.pyplot as plt 
import seaborn as sns

#propias
from utils import cargar_from_json, y_to_colors
from data_process import eval_prediccion

pd.options.mode.chained_assignment = None 


#=============== Carga configuración ===============


#cargar los valores de configuración
PROPERTIES_FILE_DIR=f"{BASE_DIR}/config/properties_load.json"
LISTA_PROPERTIES_CARGA_DATOS=["days_past_remove4housekeeping","metrica_model_decision","predict_col_name"]
#tenemos esta lista para que nos aseguremos antes de nada, que las propiedades tienen los nombres que se usan en este script
propiedades=cargar_from_json(PROPERTIES_FILE_DIR,LISTA_PROPERTIES_CARGA_DATOS)




#cargamos los ultimos datos reales y los predichos 

HISTORIC_DATA_DIR=f"{BASE_DIR}/data/historic_data.csv"
PRED_DATA_DIR=f"{BASE_DIR}/data/prediction_data.csv"
IMAGS_SAVE_DIR=f"{BASE_DIR}/results"


#vamos a crear la carpeta de results, por si no existiera de antes
if not os.path.isdir(IMAGS_SAVE_DIR):
    os.mkdir(IMAGS_SAVE_DIR)

#parametro de fecha
NOW=datetime.now()-timedelta(days=0) #la evaluación se hará para AYER , un día menos. Pues se supone que no tenemos aun los datos de hoy

#=============== Carga datos ===============


#los representamos juntos, las mismas fechas de cada uno
historic_data=pd.read_csv(HISTORIC_DATA_DIR)
historic_data["recvTime"]=pd.to_datetime(historic_data["recvTime"])
prediction_data=pd.read_csv(PRED_DATA_DIR)
prediction_data["predTime"]=pd.to_datetime(prediction_data["predTime"])
prediction_data["computationTime"]=pd.to_datetime(prediction_data["computationTime"])

#esta linea es para quedarnos solo con los datos de prediccion que tengan el mayor computationTime, para un mismo entityId y predTime
prediction_data_last = prediction_data.groupby(by=["predTime","entityId"]).apply(lambda x: x.loc[x["computationTime"].idxmax()]) 
prediction_data_last = prediction_data_last.reset_index(drop=True)

merged_data=pd.merge(prediction_data_last,historic_data,left_on=["predTime","entityId"],right_on=["recvTime","entityId"])

if merged_data.size>0:
#puede que no solapen ningunos datos, no solo los del ultimo día


    #vamos a hacer una comprobacion rapida de que todo lo anterior se ha hecho bien:
    if (merged_data.groupby(by=["predTime","entityId"]).size().unique()==np.array([1])) & (merged_data.groupby(by=["recvTime","entityId"]).size().unique()==np.array([1])):
        pass
    else:
        print("Algun error con la union de las predicciones y los datos históricos, las dimensiones no cuadran. No hay una fecha por parking y por día.")


    #nos vamos a limitar a los resultados y predicciones del ultimo día
    start_last_day_date=(NOW-timedelta(days=1)).replace(hour=0,minute=0,second=0,microsecond=0)

    end_last_day_date=start_last_day_date+timedelta(hours=23) #pongo 23 porque las incluimos ambas


    merged_data_last_day=merged_data.loc[(merged_data["recvTime"]>=start_last_day_date)&(merged_data["recvTime"]<=end_last_day_date)]
    prediction_data_last_last_day=prediction_data_last.loc[(prediction_data_last["predTime"]>=start_last_day_date)&(prediction_data_last["predTime"]<=end_last_day_date)]
    historic_data_last_day=historic_data.loc[(historic_data["recvTime"]>=start_last_day_date)&(historic_data["recvTime"]<=end_last_day_date)]


    #solo seguiremos si tenemos datos sobre los que trabajar, si este merge es 0, entonces no hacemos nada mas 

    if merged_data_last_day.size>0:
        #si nos solapan datos, primero calculamos el numero de plazas total por parking
        serie_max_plazas=historic_data_last_day.groupby("entityId")["totalSpotNumber"].agg(pd.Series.mode)

        
        fig=plt.figure(figsize=(13,13))

        #parkings unicos
        parkings_list=merged_data_last_day["entityId"].unique()

        #parametros para las flechas
        prop_green = dict(arrowstyle="-|>,head_width=0.2,head_length=0.3",
                    shrinkA=0, shrinkB=0,color="green")
        prop_red = dict(arrowstyle="-|>,head_width=0.2,head_length=0.3",
                    shrinkA=0, shrinkB=0,color="red")

        #loop por los parkings
        for i,parking in enumerate(parkings_list):
            #sacamos los datos para cada parkings

            #el merged_data_last_day solo lo usamos para hacer el cálculo de la metrica, porque si faltan datos de alguno de los dos (pred, hist),
            #queremos seguir represntando el resto
            aux_data=merged_data_last_day.loc[merged_data_last_day["entityId"]==parking]
            aux_data_pred=prediction_data_last_last_day.loc[prediction_data_last_last_day["entityId"]==parking]
            aux_data_hist=historic_data_last_day.loc[historic_data_last_day["entityId"]==parking]
            aux_data_hist_spots=aux_data_hist[propiedades["predict_col_name"]]*serie_max_plazas[parking]

            ax=plt.subplot(len(parkings_list)//2 + 1 , 2 , i+1)
                    
            #esto lo pongo para que no se permitan valores mayores a 1 ni menores a 0
            aux_error_low_bound_limited=[0 if i<=0 else i  for i in (aux_data_pred["predValue"] - aux_data_pred["predError"]) ]
            aux_error_upper_bound_limited=[serie_max_plazas[parking] if i>=serie_max_plazas[parking] else i for i in (aux_data_pred["predValue"] + aux_data_pred["predError"]) ]

            ax.fill_between(aux_data_pred["predTime_hour"], 
                            aux_error_low_bound_limited, 
                            aux_error_upper_bound_limited, 
                            alpha=0.25,color="grey")
            
            colors = y_to_colors(aux_data_hist_spots)

            ax.bar(aux_data_hist["recvTime_hour"], aux_data_hist_spots, width=0.7,color=colors)
            #sns.barplot(data=aux_data,x="recvTime_hour",y=propiedades["predict_col_name"],ax=ax)
            #sns.pointplot(data=aux_data,x="predTime_hour",y="predValue",ax=ax,color="orange",linestyles="--")
            ax.plot(aux_data_pred["predTime_hour"],aux_data_pred["predValue"],"o-",color="orange")
            x = aux_data_pred["predTime_hour"].to_numpy()
            y = aux_data_pred["predValue"].to_numpy()
            trend = aux_data_pred["predTrend_nextHour"].to_numpy()

            for i, j in enumerate(trend):
                if j == -1.0:
                    #si es negativo
                    ax.annotate("", xy=(x[i]+0.15, 2),
                                xytext=(x[i]-0.15, 15), arrowprops=prop_red)
                elif j == 1:
                    ax.annotate("", xy=(x[i]+0.15, 15),
                                xytext=(x[i]-0.15, 2), arrowprops=prop_green)
                else:
                    pass

            ax.set(xlim=(-0.25, 24.25),ylim=(-1.25, serie_max_plazas[parking]+5))
            
            
            pred_error=eval_prediccion(Y=aux_data_hist_spots.to_numpy(),Y_pred=aux_data["predValue"].to_numpy(),metricas=propiedades["metrica_model_decision"])
            ax.set_title(f"{parking} ({propiedades['metrica_model_decision']}={pred_error[propiedades['metrica_model_decision']]:.2g})")


        aux_now_str_date=start_last_day_date.strftime('%Y-%m-%d')
        fig.suptitle(f"Predicciones vs real para el día: {aux_now_str_date}",fontsize=20)
        plt.tight_layout()
        sns.despine()
        new_file_name=f"preds_vs_real_{aux_now_str_date}.png"
        print(f"Creada la imagen: {new_file_name}")
        fig.savefig(f"{IMAGS_SAVE_DIR}/{new_file_name}",dpi=72)
        plt.clf()
        plt.close(fig)
    else:
        print("No hay predicciones que solapen con datos observados.")
else:
    print("No hay predicciones que solapen con datos observados.")

#=============== HouseKeeping de imagenes ===============
print("_________ Realizando HouseKeeping de imagenes _________")
# por ultimo hacemos una rutina que va a eliminar las imagenes de {"days_past_remove4housekeeping"} 

#la fecha máxima permsitida es:
limit_past_date=(NOW-timedelta(days=propiedades["days_past_remove4housekeeping"])).replace(hour=0,minute=0,second=0,microsecond=0) #este es el ultimo dia que nos quedamos

#leemos todos los titulos de imagenes y hacemos un bucle que elimina aqeullo mas lejos de la fecha limite

for file in os.listdir(IMAGS_SAVE_DIR):
    aux_match = re.findall(r"preds_vs_real_(.+)\.png", file)
    if aux_match:
        date_of_file=datetime.strptime(aux_match[0],"%Y-%m-%d")
        #aqui pueden haber errores si no todos los ficheros tienen el nombre segun el formato, pero se han creado
        #automáticametne, por lo que no deberían.
        if date_of_file<limit_past_date:
            print(f"Eliminamos {file} por tener fecha: {date_of_file.strftime('%Y-%m-%d')}")
            #entonces eliminamos la imagen
            os.remove(f"{IMAGS_SAVE_DIR}/{file}")
print("_________HouseKeeping realizado _________")
