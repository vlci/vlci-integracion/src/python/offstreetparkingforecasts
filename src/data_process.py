import pandas as pd
#from datetime import datetime, timedelta
import numpy as np 
from typing import Optional, List, Union, Literal
from sklearn.metrics import mean_absolute_error, mean_squared_error,mean_absolute_percentage_error

#funcion que recibe lo resultante del mongo y los procesa para dejarlo en un dataframe, aggregado ademas como deseemos (minutos, horas...)import pandas as pd



def query_to_df(query:List[dict],time_col_name:str="recvTime", dropna:bool=True,add_day_week:bool=True)->pd.DataFrame:
    #pasamos la query a un df, la query ya tiene el filtrado
    # eliminamos los nans, y añadimos los dias de la semana

    #comprobamos que la query no este vacia
    assert (query is not None),  "La query/data es None"
    assert (len(query) > 0), "La query/data esta vacía"

    data=pd.DataFrame.from_dict(query)
    data[time_col_name]=pd.to_datetime(data[time_col_name]) 
    data.replace(r'^\s*$', np.nan, regex=True,inplace=True) #esto es importantisimo para pasar las cadenas vacias a nans
    if add_day_week:
        data["dayofweek"]=data[time_col_name].dt.dayofweek #The day of the week with Monday=0, Sunday=6.
    if dropna:
        data.dropna(inplace=True)

    data=data.infer_objects() #puede ser importante porque nos devuelve mucho tipo object
    #si tenemos una columna con tipos strings pero difieren en longitud, nos lo va a poner como object, da igual vaya, pero si queremos lo podemos cambiar asi_ df["..."].astype("|S")

    return data


def remove_stationals(df:pd.DataFrame,nrecords:int=5,column:str="availableSpotPercentage")->pd.DataFrame:
    #eliminamos datos que han sido durante 5 horas lo mismo, para evitar información estancada
    aux_array=df[column].to_numpy()
    #hacemos una lista de que registros cumplen la condición, lo hacemos sacando el surrouning para cada punto y comparandox
    step_aux=(nrecords-1)//2
    bool_list=np.zeros_like(aux_array,dtype=bool)
    for i in range(step_aux,aux_array.size-step_aux):
        neighbours=np.concatenate([aux_array[(i-step_aux):i], aux_array[(i+1):(step_aux+i+1)]])
        if np.all(np.concatenate([aux_array[(i-step_aux):i], aux_array[(i+1):(step_aux+i+1)]])  == aux_array[i] ):
            #eliminamos todos 
            bool_list[(i-step_aux):(step_aux+i+1)]=True
    return df[bool_list==False] # tambien vale df[~bool_list]


def group_XandY(df:pd.DataFrame,predict_col_name:str = "availableSpotPercentage",time_agg: Optional[str] = 'H')->pd.DataFrame:
    if time_agg=="H":
        df = df.groupby(df.index.hour)[predict_col_name].apply(lambda x: x.tolist())
    elif time_agg=="Min":
        df = df.groupby(df.index.minute)[predict_col_name].apply(lambda x: x.tolist())
    df=df.explode().reset_index()
    return df


def process_aggregate__data(data: List[dict],delete_stationals:bool=True, time_agg: Optional[str] = 'H',
                    time_col_name:str="recvTime",predict_col_name:str = "availableSpotPercentage", 
                    dropna:bool=True,add_day_week:bool=True,total_ocupacion_col_name:Optional[str]=None)->pd.DataFrame:
    #usada sobre la query sin agregar
    #para esta funcion estamos suponiendo que los datos ya estan en formato %, entre 0 1, si no es así,
    #debe indicarse en total_ocupacion el nombre de la columna por la que dividir predict_col_name.

    df=query_to_df(data,time_col_name=time_col_name, dropna=dropna,add_day_week=add_day_week)

    df.set_index(time_col_name, inplace=True)
    #nos aseguramos de que la columna es numerico y no tipo object

    #hacemos la division de la columna predict con el total
    if total_ocupacion_col_name is not None:
        #en la json file tendremos el campo total_ocupacion_col_name pero puede ser none,
        #si es none aqui seguiremos sin hacer nada

        #por alguna razon, esta devolviendo una columna de ints, pero lo pone como object, y son ints
        df[total_ocupacion_col_name]=df[total_ocupacion_col_name].astype("int")

        try:
            df[predict_col_name]=df[predict_col_name]/df[total_ocupacion_col_name]
        except ZeroDivisionError:
            print(f"Error con la normalización de la columna '{predict_col_name}', mira a ver si '{total_ocupacion_col_name}' tiene algún cero.")
        except Exception as e: 
            print("Error en process_data()")
            print(str(e))
    else:
        #como los datos de porcentaje estan sobre 100 y lo mejor sería trabajar sobre 1, vamos a dividir por 100 
        df[predict_col_name]=df[predict_col_name]/100.0


    if time_agg:
        df = df.resample(time_agg).mean(numeric_only=True) #'D' para diario, 'H' para horario, 'T' o 'min' para minutos, 'S' para segundos.
    
    if dropna:
        #eliminamos al agrupar tbn
        df.dropna(inplace=True)
    df=group_XandY(df,predict_col_name=predict_col_name,time_agg=time_agg)

    if delete_stationals:
        df=remove_stationals(df,column=predict_col_name)

    return df


#funcion para el cálculo de métricas
def eval_prediccion(Y:np.ndarray,Y_pred:np.ndarray,
                    metricas:Union[List[Literal["MSE","MAE","MRAE","RMSE"]],Literal["MSE","MAE","MRAE","RMSE"]]="MSE"):
    if type(metricas) is list:
        #por si ponen varias veces la misma metrica, sacamos el unique
        metricas=list(set(metricas))
    else:
        metricas=[metricas]
    result = {}
    if "MAE" in metricas:
        result["MAE"] = mean_absolute_error(Y, Y_pred)
    if "MSE" in metricas:
        result["MSE"] = mean_squared_error(Y, Y_pred)
    if "RMSE" in metricas:
        result["RMSE"] = mean_squared_error(Y, Y_pred,squared=False)
    if "MRAE" in metricas:
        result["MRAE"] = mean_absolute_error(Y, Y_pred) / Y.mean()
    if "MAPE" in metricas:
        result["MAPE"] = mean_absolute_percentage_error(Y,Y_pred)
    return result



#funcion para estimar un intervalo de error para las predicciones
#le datos los datos y nos devuelve para cada hora un valor que +- es scale x std 
def get_errors(df:pd.DataFrame,time_col_name:str="recvTime",predict_col_name:str = "availableSpotPercentage",scale:Union[float,int]=1.5):
    #recive los datos df y devuelve un array de errores para las horas de 0 a 23
    return df.groupby(by=time_col_name).std(numeric_only=True).sort_index()[predict_col_name].to_numpy()*scale


#función para crear sacar a partir de las predicciones finales, la tendencia respecto a la proxima hora
#si ahora tenemos un 50 % libre y en la proxima hay un 60 % entonces le damos un valor +1, que indica que las plazas libres 

def calc_trend_on_array(array_preds):
    #recibimos un array de x1,x2,x3...
    #devolvemos la diferencia entre x2-1, x3-x2 etc, y el ultimo es x_n-x_1
    array_aux=np.append(array_preds,array_preds[0])
    #calmulamos la diferencia
    diff_array_aux=np.diff(array_aux)
    for indx,value in enumerate(diff_array_aux):
        value=round(value,3)
        if value == 0:
            diff_array_aux[indx]=0
        elif value > 0:
            diff_array_aux[indx]=1
        elif value<0:
            diff_array_aux[indx]=-1
    return diff_array_aux