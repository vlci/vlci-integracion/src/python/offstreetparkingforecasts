# Arturo Sirvent Fresneda
# Idrica, proyecto Valencia Smart City
# 01/03/2023

# Este escript esta encargado de: 
# - cargar los datos historicos que tenemos guardados en local
# - realizar un ajuste de los datos, con distintos hyperparámetros y para distintos modelos, escogiendo el mejor modelo
# - guardar las predicciones (tanto en local, como enviadolo al CB por HTTP) y realizar housekeeping de las predicciones en local


# Luego este fichero será el que use el script main para hacer la prediccion etc.

#===================================== librerias =====================================
from os.path import dirname,abspath,join
BASE_DIR = abspath(join(dirname(__file__), '..'))
import sys
sys.path.append(f"{BASE_DIR}/src")

from data_process import get_errors, eval_prediccion,calc_trend_on_array
from utils import cargar_from_json, lista_fechas
import os
import pandas as pd
from datetime import datetime, timedelta
from transfer_data import send_data_http
from models import forecast_gaussian, forecast_gaussian2, forecast_poly
import re
import numpy as np
import traceback


# lo ponemos para que no salte un error de pandas al crear una columna
pd.options.mode.chained_assignment = None


#===================================== Carga datos =====================================



# cargar los valores de configuración
PROPERTIES_FILE_DIR = f"{BASE_DIR}/config/properties_load.json"
LISTA_PROPERTIES_CARGA_DATOS = ["predict_col_name", "past_days_considered_for_training", "days_past_remove4housekeeping", "dias_forecast", "metrica_model_decision",
                                "dict_hyperparams", "http_headers", "http_payload_names", "scale_error_std","environment","device_prefix"]
# tenemos esta lista para que nos aseguremos antes de nada, que las propiedades tienen los nombres que se usan en este script
propiedades = cargar_from_json(PROPERTIES_FILE_DIR, LISTA_PROPERTIES_CARGA_DATOS)

# carga de los secrets, entre ellos las credenciales para la conexión
SECRET_FILE_DIR = f"{BASE_DIR}/config/properties_secrets_{propiedades['environment']}.json"
LISTA_PROPIEDADES_CONEXION = ["host_mongo", "port_mongo", "user_mongo", "passwd_mongo", "database_mongo", "collection_mongo", "endpoints_CB", "http_params"]
# tenemos esta lista para que nos aseguremos antes de nada, que las propiedades tienen los nombres que se usan en este script
secrets = cargar_from_json(SECRET_FILE_DIR, LISTA_PROPIEDADES_CONEXION)


# directorios de interés
HISTORIC_DATA_DIR = f"{BASE_DIR}/data/historic_data.csv"
PRED_DATA_DIR = f"{BASE_DIR}/data/prediction_data.csv"

# cargamos los datos del historic data file
# los datos historicos deben existir, sino dará error, y bien dao
print("________ CARGA DATOS PARA LOS AJUSTES ________")
historic_df = pd.read_csv(HISTORIC_DATA_DIR)
historic_df["recvTime"] = pd.to_datetime(historic_df["recvTime"])

# Filtramos los datos para considerar solo los que estén a {past_days_considered_for_training} días de HOY {now_date}
now_date = datetime.now().replace(minute=0, second=0, microsecond=0)

if now_date.hour <= 7:
    # Si la hora actual está entre las 00 y las 7, usamos el día de hoy en lugar del de mañana
    day_of_week = now_date.strftime("%A")
else:
    # Si la hora actual es posterior a las 7, usamos el día de mañana
    tomorrow_date = now_date + timedelta(days=1)
    day_of_week = tomorrow_date.strftime("%A")

# Filtramos el dataframe según el día de la semana y si es fin de semana o no
if day_of_week in ["Saturday", "Sunday"]:
    historic_df_weekday_filtered = historic_df.loc[historic_df["recvTime"].dt.dayofweek.isin([5, 6])]
    # si etamos considerando los fines de semana, vamos a aumentar automaticamente los días a considerar,
    # para tener algunos findes más a valorar
    # si hemos dicho 14 días (2 findes), si ponemos x3 entonces serán 42 días (6 findes)
    propiedades["past_days_considered_for_training"] = propiedades["past_days_considered_for_training"] * 3
else:
    historic_df_weekday_filtered = historic_df.loc[historic_df["recvTime"].dt.dayofweek.isin([0, 1, 2, 3, 4])]


# Filtramos también por fecha
last_day_considered_for_training = now_date - timedelta(days=propiedades["past_days_considered_for_training"])

historic_df_time_filtered = historic_df_weekday_filtered.loc[historic_df_weekday_filtered["recvTime"] >= last_day_considered_for_training]

# Eliminamos el dataframe original para liberar memoria
del historic_df, historic_df_weekday_filtered

# =============== Bucles para entrenamiento ===============

print("________ CARGA DATOS OKEY ________")

# vamos a sacar todos los entytiId que tienen suficientes lecturas, por ejemplo si queremos predecir 2 días, al menos que tengan mas de 50 lecturas
parkings_valid_bool = historic_df_time_filtered.groupby(by="entityId")["entityId"].count() >  propiedades["dias_forecast"]*24
parkings_valid = parkings_valid_bool.index[parkings_valid_bool]

# vamos a guardar todas las predicciones en un diccionario para consumo propio
predicciones_total = {}

for parking in parkings_valid:
    print(
        f"======================================\n \t {parking} \n======================================")
    # bucle master de los parkings,
    # tenemos que tener un salvo conducto si falla para un parking, que se ejecute para los restantes
    try:

        # le damos a los modelos solo los datos que necesita del parking que evaluamos
        df_parking = historic_df_time_filtered.loc[historic_df_time_filtered["entityId"] == parking]
        #sacamos el valor de la ocupación máxima
        aux_max_ocupation_parking=df_parking["totalSpotNumber"].mode()[0] 
        errores_forecast = get_errors(df_parking, time_col_name="recvTime_hour",
                                      predict_col_name=propiedades["predict_col_name"], scale=propiedades["scale_error_std"])

        # puede que los datos se hayan cargado pero sean muy pocos, vmaos a comprobar que hay al menos X valores, de lo contrario nos dará errores dificiles de debuguear
        # ponemos 50 porque de normal nos pide más datos disponibles que los que se van a predecir, y vamos apredecir 48; por redondear -> 50
        assert df_parking.shape[0] > 50, "Datos insuficientes para la predicción." #comprobación redundate, solo por si acaso

        # predicciones basadas en los datos

        # hacemos una ejecución de los algoritmos desponibles para diferentes hiperparámetros
        modelos = [forecast_gaussian, forecast_gaussian2, forecast_poly]
        # no es necesario calcular todas las metricas si luego solo vamos a usar una como criterio de elección, pero lo dejamos asi por ahora
        metricas_calcular = ["MSE", "MAE", "MRAE", "RMSE"]
        results_fit = []
        # iteramos entre los modelos
        print("________ EMPEZAMOS LOS AJUSTES ________")

        for index, modelo in enumerate(modelos):
            print(f"Modelo: {modelo.__name__}")
            # for's anidados para probar todas las combinaciones
            for maxfev in propiedades["dict_hyperparams"]["maxfev"]:
                print(f"\t Con paramentros:\n \t \t maxfev:{maxfev}")
                for x_shift in propiedades["dict_hyperparams"]["x_shift"]:
                    print(f"\t \t \t x_shift={x_shift}")
                    # para cada combinacion, calculamos una métrica de que tal se ajusta a la media de los datos
                    # es muy probable que para algunos no funcione y no encuentre buena combiancion, entonces
                    # para ello tenemos que anyadir un try
                    try:
                        # sacamos los modelos
                        X_train = df_parking["recvTime_hour"].to_numpy()
                        Y_train = df_parking[propiedades["predict_col_name"]].to_numpy()
                        # queremos que devuelva el modelo entrenado
                        fitted_model = modelo(X_train, Y_train, n_days_pred=None, x_shift=x_shift, maxfev=maxfev)
                    except RuntimeError:
                        # si no logra ajustar un modelo, no pasa nada, a la siguiente configuracion
                        pass
                    except Exception as e:
                        # si es un error diferente, bueno, lo mostramos, pero que no pare
                        print(f"Error inesperado {e.with_traceback}")
                        traceback.print_exc()  # imprimir la traza del error

                    else:  # se ejecuta si ha sido satisfactorio sin excepciones

                        # calculamos las metricas
                        # el cálculo lo realizamos entre las medias del valor para cada hora, y la predicción para ese valor
                        Y_avg_real = df_parking.groupby(by=df_parking["recvTime_hour"])[propiedades["predict_col_name"]].mean(numeric_only=True).to_numpy()
                        Y_pred = fitted_model(df_parking["recvTime_hour"].sort_values().unique())
                        metric_aux = eval_prediccion(Y_avg_real, Y_pred, metricas=metricas_calcular)
                        results_fit.append({"model_name": modelo.__name__, "fitted_model": fitted_model,"metricas": metric_aux, "hyper_params": {"x_shift": x_shift, "maxfev": maxfev}})

        print("________ AJUSTES REALIZADOS ________")

        print("________ EVALUANDO MEJOR RESULTADO ________")
        # escogemos la mejor de las opciones
        # propiedades["metrica_model_decision"] es la metrica que va a establecer el criterio de cual es la mejor opción
        valores_metrica = [element["metricas"][propiedades["metrica_model_decision"]] for element in results_fit]

        # ahora escogemos el modelo cuyo valores_metrica es menor
        index_best_model = np.argmin(valores_metrica)
        best_model = results_fit[index_best_model]["fitted_model"]
        print("________ EVALUANDO RESULTADO OBTENIDO ________")

        # por último, calculamos los días pedidos
        # esto son las horas a predecir, si son dos dias, son dos veces lo mismo, de 0 a 23
        horas_forecast = np.tile(np.arange(0, 24), propiedades["dias_forecast"])
        print("________ REALIZANDO PREDICCIÓN ________")
        final_forecast = best_model(horas_forecast)

        #como estamos tratando con porcentajes, no podemos sobrepasar 1 de ocupación (y 0 por abajo), pero la predicción podría erroneamente hacerlo.
        #así que lo vamos a limitar ahora, nada mas se ha hecho la predicción
        final_forecast=np.where(final_forecast>1,1,final_forecast) #si es mayor a 1, ponemos 1
        final_forecast=np.where(final_forecast<0,0,final_forecast) # si es menor a 0, ponemos 0

        #y sacamos la trend de cada punto de la prediccion
        trend_forecast = calc_trend_on_array(final_forecast)


        #vamos a pasar de porcentajes el error y el valor, a numero de plazas
        final_forecast=final_forecast*aux_max_ocupation_parking
        errores_forecast=errores_forecast*aux_max_ocupation_parking

        print("________ PREDICCIÓN OKEY ________")

        # y como tenemos los errores para solo un dia 0-23, duplicamos el array para abarcar mas dias
        final_forecast_errors = np.tile(errores_forecast, propiedades["dias_forecast"])

        # y formateamos los resultados para enviarlos

        # ¡¡MUY IMPORTANTE!!! -->  tenemos que asignarle unas fechas a los VALORES PREDICHOS, se le van a asignar valores de horas desde 0 a 23, y las predicciones serán para los dias siguientes AL ACTUAL
        # y la idea es que la prediccion se haga para el dia siguiente, pero si se hiciera de madrugada, horas entre 0-7 h, entonces vamos a tomar ese mismo día.
        # esto son la fechas completas correspondientes a las predicciones
        fechas_forecast = lista_fechas(num_preds=len(horas_forecast), last_datetime=datetime.now())

        # creamos el payload, una lista con cada chunk que enviaremos
        data_payload = []
        dateNow = datetime.now()
        for error, fecha_pred, valor_pred,trendPred in zip(final_forecast_errors, fechas_forecast, final_forecast, trend_forecast):
            data_payload.append({
                                propiedades["http_payload_names"]["predTime"]: fecha_pred.strftime("%Y-%m-%dT%H:%M:%S"),
                                propiedades["http_payload_names"]["predValue"]: round(valor_pred),
                                propiedades["http_payload_names"]["predError"]: round(error),
                                propiedades["http_payload_names"]["computationTime"]: dateNow.strftime("%Y-%m-%dT%H:%M:%S"),
                                propiedades["http_payload_names"]["predTrend_nextHour"]: trendPred
                                })

        # conformamos los datos a enviar
        print("________ SE PROCEDE AL ENVIO ________")

        # tambien anyadimos el device de ese parking para el CB
        numero_parking_aux = re.findall(pattern=r"\d+$", string=parking)[0]
        
        if (propiedades['environment'] =="PRO") or (propiedades['environment'] =="PROD"):
            # si es pro, el device no tiene PRO explicitamente
            aux_prefix_device=f"{propiedades['device_prefix']}"
        else:
            aux_prefix_device=f"{propiedades['device_prefix']}{propiedades['environment']}"
        secrets["http_params"]["i"] = f"{aux_prefix_device}_{numero_parking_aux}" # POR EJEMPLO esto en pre será : devParkingForecastPRE_{id}
        #se hacen los POSTS
        send_data_http(endpoint=secrets["endpoints_CB"], list_payloads=data_payload,
                       params=secrets["http_params"], headers=propiedades["http_headers"])

        print("________ ENVIO REALIZADO ________")

    except Exception as e:
        # si falla algo anterior, lo mostramos
        print(f"\n \n -----> Error inesperado {e.with_traceback}")
        traceback.print_exc()  # imprimir la traza del error

    predicciones_total[parking] = data_payload

# =============== Guardamos los datos de la predicción para el monitoreo ===============

print("========== GUARDANDO PREDICCIONES EN LOCAL ========== ")

# vamos a guardar las predicciones para hacer el monitoreo
df_preds = {i: pd.DataFrame(predicciones_total[i]) for i in predicciones_total}
# y le añadimos la columna de entityId
for id_parking in predicciones_total:
    df_preds[id_parking]["entityId"] = id_parking #añadimos la columna 
    # esto por si queremos poner en formato fecha las columnas fechas
    df_preds[id_parking]["predTime"] = pd.to_datetime(df_preds[id_parking]["predTime"])
    df_preds[id_parking]["computationTime"] = pd.to_datetime(df_preds[id_parking]["computationTime"])

new_predictions = pd.concat(df_preds.values())
new_predictions["predTime_hour"] = new_predictions["predTime"].dt.hour


# ahora cargamos las predicciones anteriores y debemos de incluir las nuevas
# primero nos aseguramos que existe un archivo asi, sino, pues solo lo creamos y ya

if os.path.exists(PRED_DATA_DIR):

    old_pred_df = pd.read_csv(PRED_DATA_DIR)
    old_pred_df["predTime"] = pd.to_datetime(old_pred_df["predTime"])
    old_pred_df["computationTime"] = pd.to_datetime(old_pred_df["computationTime"])

    # y las agregamos juntas
    historic_preds = pd.concat([old_pred_df, new_predictions]).drop_duplicates()

    # y eliminamos aquellas que tengan varios meses de antigüedad en el tiempo de prediccion
    limit_past_date = (datetime.now()-timedelta(days=propiedades["days_past_remove4housekeeping"])).replace(hour=0, minute=0, second=0, microsecond=0)  # este es el ultimo dia que nos quedamos

    historic_preds = historic_preds.loc[historic_preds["predTime"]>= limit_past_date]
    print("HOUSEKEEPING DE PREDICCIONES LOCAL OKEY")
    # y las guardamos

    historic_preds.to_csv(PRED_DATA_DIR, index=False)

else:
    # si no existe lo guardamos directamente
    new_predictions.to_csv(PRED_DATA_DIR, index=False)

print("========== GUARDANDO PREDICCIONES EN LOCAL REALIZADO ========== ")
