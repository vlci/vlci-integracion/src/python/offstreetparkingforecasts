# Arturo Sirvent Fresneda
# Idrica, proyecto Valencia Smart City
# 01/03/2023

# Este escript esta encargado de: 
# - leer de un fichero los ultimos registros históricos (local) 
# - cargar los ultimos datos agregados de los que no se tienen registro (mongo)
# - añadirlos al fichero mencionado (local)

# Luego este fichero será el que use el script main para hacer la prediccion etc.

#===================================== librerias =====================================
from os.path import dirname,abspath,join
BASE_DIR = abspath(join(dirname(__file__), '..'))
#BASE_DIR="/opt/etls/sc_vlci/PRE/py_oci_parkings_forecast"

import sys
sys.path.append(f"{BASE_DIR}/src")


# basicas
from datetime import datetime,timedelta
import os 
import pandas as pd 
import numpy as np 
#propias
from transfer_data import read_mongo_aggregate
from utils import cargar_from_json
from data_process import query_to_df


#===================================== código  =====================================

#carga de los secrets, entre ellos las credenciales para la conexión

#cargar los valores de configuración
PROPERTIES_FILE_DIR=f"{BASE_DIR}/config/properties_load.json"
LISTA_PROPIEDADES_CARGA_DATOS=["predict_col_name","total_ocupacion_col_name","past_days_considered_for_download","pipeline_mongo","days_past_remove4housekeeping","environment"]
#tenemos esta lista para que nos aseguremos antes de nada, que las propiedades tienen los nombres que se usan en este script
propiedades=cargar_from_json(PROPERTIES_FILE_DIR,LISTA_PROPIEDADES_CARGA_DATOS)


SECRET_FILE_DIR = f"{BASE_DIR}/config/properties_secrets_{propiedades['environment']}.json"
LISTA_PROPIEDADES_CONEXION=["host_mongo", "port_mongo", "user_mongo", "passwd_mongo", "database_mongo", "collection_mongo"] 
#tenemos esta lista para que nos aseguremos antes de nada, que las propiedades tienen los nombres que se usan en este script
secrets=cargar_from_json(SECRET_FILE_DIR,LISTA_PROPIEDADES_CONEXION)


#vamos a crear la carpeta de data, por si no existiera de antes
if not os.path.isdir(f"{BASE_DIR}/data"):
    os.mkdir(f"{BASE_DIR}/data")

HISOTRIC_DATA_DIR=f"{BASE_DIR}/data/historic_data.csv"


# leemos el archivo de datos históricos

#vamos a cargar a partir de este valor, y hasta now() 
now_date=datetime.now().replace(minute=0,second=0,microsecond=0)
now_date_text=(datetime.now().replace(minute=0,second=0,microsecond=0)).strftime("%Y-%m-%dT%H:%M:%S") # como tenemos $gt en la fecha final y no $gte no hace falta poner .now()-timedelta(hours=1)

print("________ LEYENDO DATOS EN HISTÓRICO ________") 

# primero combrobamos que el archivo histórico existe
if os.path.exists(HISOTRIC_DATA_DIR):

    historic_data=pd.read_csv(HISOTRIC_DATA_DIR)
    print("________ LEIDOS CORRECTAMENTE ________") 

    #convertimos las fechas al tipo correcto
    historic_data["recvTime"]=pd.to_datetime(historic_data["recvTime"]) 
    total_parkings_historic=historic_data['entityId'].unique()

    # tomamos la ultima fecha desponible de recvTime
    aux_last_week_time=(now_date-timedelta(days=propiedades["past_days_considered_for_download"]))

    #hacemos lo siguiente para que si fallara en sacar la fecha smáxima 

    #last_recvTime= aux_date if not pd.isnull(aux_date:=historic_data.loc[historic_data["recvTime"]>=aux_last_week_time].groupby(by="entityId")["recvTime"].max().min()) else datetime.now()-timedelta(days=propiedades["days_past_remove4housekeeping"]) #esto saca el menor de los máximos para cada parking
    if (historic_data.loc[historic_data["recvTime"]>=aux_last_week_time]).empty:
        last_recvTime = aux_last_week_time
    else:
        last_recvTime=historic_data.loc[historic_data["recvTime"]>=aux_last_week_time].groupby(by="entityId")["recvTime"].max().min() #esto saca el menor de los máximos para cada parking
    #esta última linea es un poco peligrosa porque puede ser que esté tomando una fecha muy antigua de un parking que ya no está disponible
    #como se entiende que no nos interesan los parkings que no actualizan, vamos a hacer la búsqueda anterior sobre los parkings disponibles la ÚLTIMA SEMANA (past_days_considered_for_download = 7días)
    print(f"Tenemos histórico de los parkings: {total_parkings_historic}")

    #y vamos a hacer una comprobación muy sencilla: ¿hay algun parking que este en el total, pero que no se considere ya por no estar en la última semana?
    aux_parking_last_week=historic_data.loc[historic_data["recvTime"]>=aux_last_week_time,"entityId"].unique()
    #que parkings estan en{total_parkings_historic} y no en {aux_parking_last_week}
    set_aux_parking_last_week = set(aux_parking_last_week)
    set_total_parkings_historic = set(total_parkings_historic)
    parkings_in_total_but_not_in_aux = set_total_parkings_historic.difference(set_aux_parking_last_week)

    if len(parkings_in_total_but_not_in_aux)>0:
        #si tenemos algun elemento, es porque tenemos parking que llevan tiempo sin actualizar datos 
        print(f"Y de los siguientes parkings llevamos al menos {propiedades['past_days_considered_for_download']} días sin tener datos: {parkings_in_total_but_not_in_aux}")

    last_recvTime=last_recvTime + timedelta(hours=1) # queremos a partir de la ultima hora disponible
    last_recvTime_text=datetime.strftime(last_recvTime,"%Y-%m-%dT%H:%M:%S")
else:
    #si no existe, vamos a tomar como ultimo punto a cargar el límite que tomamos para el borrado de datos
    last_recvTime=datetime.now()-timedelta(days=propiedades["days_past_remove4housekeeping"])
    last_recvTime_text=datetime.strftime(datetime.now()-timedelta(days=propiedades["days_past_remove4housekeeping"]),"%Y-%m-%dT%H:%M:%S")
    print(f"________ NO HAY HISTÓRICO, SE DESCARGARÁN LOS {propiedades['days_past_remove4housekeeping']} DÍAS PASADOS  ________") 




# quizá hay un problema, y la carga se esta ejecutando cuando no debe, o varias veces, y entonces no habrá nada que descargar
# solo lo vamos a descargar si entre las fechas hay almenos una hora de diferencia

if (now_date-last_recvTime)>timedelta(hours=1):

    # ahora vamos a cargar desde mongo los datos faltantes

    #hacemos un bucle por si la carga fuera muy pesada, para ver el progreso de la descarga
    time_intervals = np.arange(last_recvTime,now_date, timedelta(days=3)).astype(datetime)
    #ponemos la ultima fecha (now_date) si aun no la ha puesto
    if time_intervals[-1]!= now_date:
        time_intervals=np.append(time_intervals,now_date)

    

    #vamos a descargar datos si almenos hay una hora que descargar
    print("________ DESCARGANDO DATOS RECIENTES ________") 
    print(f"En total es desde: {last_recvTime}, hasta: {now_date} \n")



    #leemos los ultimos datos, lo vamos a hacer por batches
    full_data=[]
    for i in np.arange(1,len(time_intervals)):
        print(f"\t Ciclo: {i}/{len(time_intervals)-1}, Init: {time_intervals[i-1]}, End: {time_intervals[i]}")
        #fechas desde - hasta
        propiedades["pipeline_mongo"][0]["$match"]["recvTime"]["$gt"]=time_intervals[i-1]
        propiedades["pipeline_mongo"][0]["$match"]["recvTime"]["$lte"]=time_intervals[i]
        #descarga de los datos desde mongo
        data_aux=read_mongo_aggregate(host_mongo=secrets["host_mongo"],port_mongo=secrets["port_mongo"],
                    database_mongo=secrets["database_mongo"],collection_mongo=secrets["collection_mongo"],
                    user_mongo=secrets["user_mongo"],passwd_mongo=secrets["passwd_mongo"],pipeline_mongo= propiedades["pipeline_mongo"])
        full_data.extend(data_aux)

    print("________ DESCARGA TERMINADA ________") 

    #damos forma a los datos
    print("________ MANIPULANDO LA QUERY ________") 

    #a estos nuevos datos les tenemos que aplicar la misma transformacion
    df=query_to_df(full_data)
    df[propiedades["predict_col_name"]]=round(df[propiedades["predict_col_name"]]/100.0,3) #lo dividimos porque esta en tanto porciento lo quiero de 0 a 1.
    df["recvTime_hour"]=df["recvTime"].dt.hour #añadimos una columna con las horas
    if os.path.exists(HISOTRIC_DATA_DIR):
        #si existe el archivo entonces los mezclamos como toca
        df2 = pd.concat([historic_data, df], axis=0, ignore_index=True)

        df3 = df2.drop_duplicates(subset=["recvTime","entityId"]).sort_values(["entityId","recvTime"])

        #vamos a añadir un purgado de fechas que sean "days_past_remove4housekeeping" meses atras a la fecha actual, y asi nos quitamos de rollos de memoria.

        limit_past_date=(datetime.now()-timedelta(days=propiedades["days_past_remove4housekeeping"])).replace(hour=0,minute=0,second=0,microsecond=0) #este es el ultimo dia que nos quedamos
        print("HOUSEKEEPING DEL HISTORICO LOCAL OKEY")
        #eliminamos los datos pasados de aquí
        df4=df3.loc[df3["recvTime"]>=limit_past_date]

    else:
        #si no existia el archivo, pues lo guardamos y ya 
        df4=df

    print("________ FINALIZADA LA MANIPULACIÓN ________") 

    print("________ GUARDANDO EL HISTÓRICO ________") 
    #lo guardamos
    df4.to_csv(HISOTRIC_DATA_DIR,index=False)

    print("________ HISTÓRICO GUARDADO ________") 

else:
    print("Lo últimos datos del registro y el datetime.now() estan a menos de 1 hora de distancia, no hay datos que descargar.")